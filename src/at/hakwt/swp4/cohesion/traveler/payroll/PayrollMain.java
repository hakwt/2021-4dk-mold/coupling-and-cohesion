package at.hakwt.swp4.cohesion.traveler.payroll;

public class PayrollMain {

    public static void main(String[] args) {
        PayrolCalculator calculator = new PayrolCalculator();
        Employee ferdi = new Employee(2000, "Ferdinand");
        Expense diaeten = new Expense(24);
        Expense kmGeld = new Expense(42); // 100 km * 0,42 C
        Double salary = calculator.getSalary(ferdi, 10, new Expense[]{diaeten, kmGeld});
        calculator.print(ferdi, salary, "November 2022");


        // mit Import
        PayrollData data = calculator.importData("daten.csv");
        salary = calculator.getSalary(data.getEmployee(), data.getNoOfOverhours(), data.getExpenses());
        calculator.print(data.getEmployee(), salary, "Dezember 2022");


    }

}
