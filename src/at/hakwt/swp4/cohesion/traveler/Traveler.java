package at.hakwt.swp4.cohesion.traveler;

public class Traveler {

    public void startJourney() {
        // ...
    }

    public void maintainCar() {
        // put car into garage
        // buy maintainacne parts
        // do maintainance work
    }

    public void cook() {
        // find place to do cooking
        // buy ingredients
        // do the cooking
    }

}
