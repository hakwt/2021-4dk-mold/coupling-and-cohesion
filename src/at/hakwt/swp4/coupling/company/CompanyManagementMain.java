package at.hakwt.swp4.coupling.company;

public class CompanyManagementMain {

    public static void main(String[] args) {
        Worker karli = new Worker();
        Worker fredi = new Worker();
        Manager robert = new Manager(new Worker[]{karli, fredi});
        robert.manageWork();

        /*
            - Mit dieser Struktur kann ein Manager nur "Arbeiter" managen.
            - Es gibt in dem Unternehmen auch Buchhalter (=Accountant). Durch Umstrukturierungen soll "robert"
              jetzt auch Buchhalter managen können.
            - Überlege und implementiere eine Lösung mit der Manager von Arbeiter entkoppelt werden, sodass ein
              Manager Arbeiter und Buchhalter managen kann ... und was sich sonst noch in dem Unternehmen entwickelt
         */
    }

}
