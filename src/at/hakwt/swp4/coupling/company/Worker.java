package at.hakwt.swp4.coupling.company;

/**
 * Worker represents "Arbeiter"
 */
public class Worker {

    public void process(String[] instructions) {
        for (String instruction: instructions) {
            System.out.println("Working on " + instruction);
        }
    }

}
